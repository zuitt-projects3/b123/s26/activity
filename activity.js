let http = require("http");
http.createServer(function(request,response){
  console.log(request.url)
    if(request.url === "/"){
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end('Welcome to Our Page!');
  } else if(request.url === "/login"){
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end('Welcome to the Login Page. Please Log in your credentials.');
  } else if(request.url === "/register"){
    response.writeHead(200,{'Content-Type': 'text/plain'});
    response.end('Welcome to the Register Page. Please register your details');
  } else {
    response.writeHead(404,{'Content-Type': 'text/plain'});
    response.end('Resources not found');
  }
}).listen(8000);

console.log(`Server running at localHost:8000`);
